export default context => {
	console.log('[middlewaer] Just auth!')

	if (!context.store.getters.isAuthenticated) {
		context.redirect('/admin/auth')
	}
}
