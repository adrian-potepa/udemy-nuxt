import Vuex from 'vuex';
import Cookie from 'js-cookie';

const createStore = () => {
	return new Vuex.Store({
		state: {
			loadedPosts: [],
			token: null
		},
		mutations: {
			setPosts(state, posts) {
				state.loadedPosts = posts;
			},
			addPost(state, post) {
				state.loadedPosts.push(post);
			},
			editPost(state, editedPost) {
				const postIndex = state.loadedPosts.findIndex(post => post.id === editedPost.id);

				state.loadedPosts[postIndex] = editedPost;
			},
			setToken(state, token) {
				state.token = token;
			},
			clearToken(state) {
				state.token = null;
			}
		},
		actions: {
			nuxtServerInit(vuexContext, context) {
				return context.app.$axios.$get('/posts.json')
					.then(data => {
						let postArray = [];

						for (const key in data) {
							postArray.push({ ...data[key], id: key });
						}

						vuexContext.commit('setPosts', postArray)
					})
					.catch(e => context.error(e));
			},
			addPost(vuexContext, post) {
				const createdPost = {
					...post,
					updatedDate: new Date()
				};

				return this.$axios.$post(process.env.baseUrl + '/posts.json', createdPost)
					.then(data => {
						vuexContext.commit('addPost', { ...createdPost, id: data.name });
					})
					.catch(e => console.log(e));
			},
			editPost(vuexContext, post) {
				const editedPost = {
					...post,
					updatedDate: new Date()
				};

				return this.$axios.$put(`${process.env.baseUrl}/posts/${post.id}.json?auth=${vuexContext.state.token}`, editedPost)
					.then(result => {
						vuexContext.commit('editPost', editedPost);
					})
					.catch(e => console.log(e));
			},
			setPosts(vuexContext, posts) {
				vuexContext.commit('setPosts', posts);
			},
			authenticateUser(vuexContext, authData) {
				let authUrl = `https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=${process.env.firebaseKey}`;

				if (!authData.isLogin) {
					authUrl = `https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=${process.env.firebaseKey}`;
				}

				return this.$axios
					.$post(authUrl, {
						email: authData.email,
						password: authData.password,
						returnSecureToken: true
					})
					.then(result => {
						vuexContext.commit('setToken', result.idToken);
						localStorage.setItem('token', result.idToken)
						localStorage.setItem('tokenExpiration', new Date().getTime() + +result.expiresIn * 1000);
						Cookie.set('token', result.idToken);
						Cookie.set('tokenExpiration',  new Date().getTime() + +result.expiresIn * 1000);

						return this.$axios.$post('http://localhost:3000/api/track-data', {data: 'Authenticated!'})
					}).catch(err => {
						console.log(err);
					});
			},
			initAuth(vuexContext, req) {
				let token;
				let expirationDate;

				if (req) {
					if (!req.headers.cookie) {
						return;
					}

					const jwtCookie = req.headers.cookie
						.split(';')
						.find(c => c.trim().startsWith('jwt='));

					if (!jwtCookie) {
						return;
					}

					token = jwtCookie.split('=')[1];

					console.log(req.headers.cookie
						.split(';')
						.find(c => c.trim().startsWith('tokenExpiration=')))

					expirationDate = req.headers.cookie
						.split(';')
						.find(c => c.trim().startsWith('tokenExpiration='))
						.split('=')[1];
				} else if (process.client) {
					token = localStorage.getItem('token');
					expirationDate = localStorage.getItem('tokenExpiration');
				}

				if (new Date().getTime() > +expirationDate || !token) {
					vuexContext.dispatch('logout');
					return;
				}

				vuexContext.commit('setToken', token);
			},
			logout(vuexContext) {
				vuexContext.commit('clearToken');
				Cookie.remove('token');
				Cookie.remove('tokenExpiration');

				if (process.client) {
					localStorage.removeItem('token');
					localStorage.removeItem('tokenExpiration');
				}
			}
		},
		getters: {
			loadedPosts(state) {
				return state.loadedPosts;
			},
			isAuthenticated(state) {
				return state.token != null;
			}
		}
	});
};

export default createStore;
